<?php

namespace Drupal\kic_front\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Provides a 'About' block.
 *
 * @Block(
 *   id = "kic_front_category_navigation_block",
 *   admin_label = @Translation("KIC Frontpage Category Navigation"),
 *   category = @Translation("Frontpage"),
 * )
 */
class CategoryNavigationBlock extends BlockBase implements BlockPluginInterface {

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build() {
    $config = $this->getConfiguration();

    $element = [
      '#theme' => 'kic_front_category_navigation',
      '#heading' => $config['heading'] ?? '',
      '#items' => [],
    ];
    foreach ($this->itemIndices() as $position) {
      $text_value_name = 'text_' . $position;
      $anchor_value_name = 'anchor_' . $position;
      $button_text_value_name = 'button_text_' . $position;
      $button_link_value_name = 'button_link_' . $position;
      $item = [
        'text' => $config[$text_value_name] ?? '',
        'anchor' => $config[$anchor_value_name] ?? '',
        'button_text' => $config[$button_text_value_name] ?? '',
        'button_link' => $config[$button_link_value_name] ?? '',
      ];
      $element['#items'][] = $item;
    }
    return $element;
  }

  /**
   * Returns the configuration form elements specific to this block plugin.
   *
   * Blocks that need to add form elements to the normal block configuration
   * form should implement this method.
   *
   * @param array $form
   *   The form definition array for the block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The renderable form array representing the entire configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['heading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading'),
      '#description' => $this->t('Heading of the "about" block.'),
      '#default_value' => isset($config['heading']) ? $config['heading'] : '',
    ];

    foreach ($this->itemIndices() as $position) {
      $text_value_name = 'text_' . $position;
      $anchor_value_name = 'anchor_' . $position;
      $button_text_value_name = 'button_text_' . $position;
      $button_link_value_name = 'button_link_' . $position;

      $text_title = ucfirst($position) . ' text';
      $anchor_title = ucfirst($position) . ' anchor';
      $button_text_title = ucfirst($position) . ' button text';
      $button_link_title = ucfirst($position) . ' button link';

      $text_description = 'Text of the item on the ' . $position . '.';
      $anchor_description = 'Anchor the item on the ' . $position . ' does link to.';
      $button_text_description = 'Button text of the item on the ' . $position . '.';
      $button_link_description = 'Button link of the item on the ' . $position . '.';

      $form[$text_value_name] = [
        '#type' => 'textarea',
        '#title' => $this->t($text_title),
        '#description' => $this->t($text_description),
        '#default_value' => $config[$text_value_name] ?? '',
      ];
      $form[$anchor_value_name] = [
        '#type' => 'textfield',
        '#title' => $this->t($anchor_title),
        '#description' => $this->t($anchor_description),
        '#default_value' => $config[$anchor_value_name] ?? '',
      ];
      $form[$button_text_value_name] = [
        '#type' => 'textfield',
        '#title' => $this->t($button_text_title),
        '#description' => $this->t($button_text_description),
        '#default_value' => $config[$button_text_value_name] ?? '',
      ];
      $form[$button_link_value_name] = [
        '#type' => 'textfield',
        '#title' => $this->t($button_link_title),
        '#description' => $this->t($button_link_description),
        '#default_value' => $config[$button_link_value_name] ?? '',
      ];
    }

    return $form;
  }

  /**
   * Adds block type-specific submission handling for the block form.
   *
   * Note that this method takes the form structure and form state for the full
   * block configuration form as arguments, not just the elements defined in
   * BlockPluginInterface::blockForm().
   *
   * @param array $form
   *   The form definition array for the full block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\Core\Block\BlockPluginInterface::blockForm()
   * @see \Drupal\Core\Block\BlockPluginInterface::blockValidate()
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['heading'] = $values['heading'];
    foreach ($this->itemIndices() as $position) {
      $text_value_name = 'text_' . $position;
      $anchor_value_name = 'anchor_' . $position;
      $button_text_value_name = 'button_text_' . $position;
      $button_link_value_name = 'button_link_' . $position;

      $this->configuration[$text_value_name] = $values[$text_value_name] ?? '';
      $this->configuration[$anchor_value_name] = $values[$anchor_value_name] ?? '';
      $this->configuration[$button_text_value_name] = $values[$button_text_value_name] ?? '';
      $this->configuration[$button_link_value_name] = $values[$button_link_value_name] ?? '';
    }
  }

  /**
   * Get the indices/positions of the 'about'-items.
   *
   * @return array
   *   An array of string indices.
   */
  protected function itemIndices() {
    return ['left', 'middle', 'right'];
  }

}
