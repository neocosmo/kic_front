<?php

namespace Drupal\kic_front\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Provides a 'About' block.
 *
 * @Block(
 *   id = "kic_front_about_block",
 *   admin_label = @Translation("KIC Frontpage About"),
 *   category = @Translation("Frontpage"),
 * )
 */
class AboutBlock extends BlockBase implements BlockPluginInterface {

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build() {
    $config = $this->getConfiguration();

    $element = [
      '#theme' => 'kic_front_about',
      '#heading' => $config['heading'] ?? '',
      '#items' => [],
      '#register' => [
        '#type' => 'link',
        '#title' => $this->t('Register now for free'),
        '#url' => Url::fromRoute('user.register', [], [
          'attributes' => [
            'class' => ['btn', 'hover-bg-green'],
          ],
        ]),
      ],
    ];
    foreach ($this->itemIndices() as $position) {
      $text_value_name = 'text_' . $position;
      $icon_value_name = 'icon_' . $position;
      if (empty($config[$text_value_name])) {
        continue;
      }
      $item = [
        'text' => $config[$text_value_name],
      ];
      $icon_fid = $config[$icon_value_name][0] ?? 0;
      $icon_file = File::load($icon_fid);
      if ($icon_file) {
        $item['image'] = [
          '#theme' => 'image',
          '#uri' => $icon_file->getFileUri(),
        ];
      }
      $element['#items'][] = $item;
    }
    return $element;
  }

  /**
   * Returns the configuration form elements specific to this block plugin.
   *
   * Blocks that need to add form elements to the normal block configuration
   * form should implement this method.
   *
   * @param array $form
   *   The form definition array for the block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The renderable form array representing the entire configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['heading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading'),
      '#description' => $this->t('Heading of the "about" block.'),
      '#default_value' => isset($config['heading']) ? $config['heading'] : '',
    ];

    foreach ($this->itemIndices() as $position) {
      $text_name = 'text_' . $position;
      $text_title = ucfirst($position) . ' text';
      $text_description = 'Text of the item on the ' . $position . '.';
      $form[$text_name] = [
        '#type' => 'textfield',
        '#title' => $this->t($text_title),
        '#description' => $this->t($text_description),
        '#default_value' => isset($config[$text_name]) ? $config[$text_name] : '',
      ];

      $icon_name = 'icon_' . $position;
      $icon_title = ucfirst($position) . ' icon';
      $icon_description = 'Text of the item on the ' . $position . '.';
      $form[$icon_name] = [
        '#type' => 'managed_file',
        '#title' => $this->t($icon_title),
        '#description' => $this->t($icon_description),
        '#default_value' => isset($config[$icon_name]) ? $config[$icon_name] : '',
        '#upload_validators' => [
          'file_validate_is_image' => [],
          'file_validate_image_resolution' => ['640x640', '180x180'],
        ],
        '#upload_location' => 'public://kic_front/about',
      ];
    }

    return $form;
  }

  /**
   * Adds block type-specific submission handling for the block form.
   *
   * Note that this method takes the form structure and form state for the full
   * block configuration form as arguments, not just the elements defined in
   * BlockPluginInterface::blockForm().
   *
   * @param array $form
   *   The form definition array for the full block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\Core\Block\BlockPluginInterface::blockForm()
   * @see \Drupal\Core\Block\BlockPluginInterface::blockValidate()
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['heading'] = $values['heading'];
    foreach ($this->itemIndices() as $position) {
      $text_name = 'text_' . $position;
      $this->configuration[$text_name] = $values[$text_name] ?? '';

      $icon_name = 'icon_' . $position;
      $old_fid = $this->configuration[$icon_name][0] ?? 0;
      $new_fid = $values[$icon_name][0] ?? 0;
      /*
       * Remove the old file.
       * Basically we can assign usages to the files and keeping and deleting
       * files should be done based on the usages. But usages are intended only
       * for entities and a block is not an entity, so this does not work here
       * and we keep order manually.
       */
      if ($old_fid !== 0 && $old_fid != $new_fid) {
        $old_file = File::load($old_fid);
        if ($old_file) {
          $old_file->delete();
        }
      }
      $this->configuration[$icon_name] = $values[$icon_name] ?? [];
      // Make the new file permanent.
      $new_file = File::load($new_fid);
      if ($new_file) {
        $new_file->setPermanent();
        $new_file->save();
      }
    }
  }

  /**
   * Get the indices/positions of the 'about'-items.
   *
   * @return array
   *   An array of string indices.
   */
  protected function itemIndices() {
    return ['left', 'middle', 'right'];
  }

}
