<?php

namespace Drupal\kic_front\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Provides a 'CategoryHeader' block.
 *
 * @Block(
 *   id = "kic_front_category_header_block",
 *   admin_label = @Translation("KIC Frontpage Category Header"),
 *   category = @Translation("Frontpage"),
 * )
 */
class CategoryHeaderBlock extends BlockBase implements BlockPluginInterface {

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build() {
    $config = $this->getConfiguration();

    $element = [
      '#theme' => 'kic_front_category_header',
      '#kicker' => $config['kicker'] ?? '',
      '#heading' => $config['heading'] ?? '',
      '#text' => $config['text'] ?? '',
    ];
    $image_fid = $config['image'][0] ?? 0;
    $image_file = File::load($image_fid);
    if ($image_file) {
      $element['#image'] = [
        '#theme' => 'image',
        '#uri' => $image_file->getFileUri(),
      ];
    }
    return $element;
  }

  /**
   * Returns the configuration form elements specific to this block plugin.
   *
   * Blocks that need to add form elements to the normal block configuration
   * form should implement this method.
   *
   * @param array $form
   *   The form definition array for the block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The renderable form array representing the entire configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['kicker'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Kicker'),
      '#description' => $this->t('Kicker of the block.'),
      '#default_value' => isset($config['kicker']) ? $config['kicker'] : '',
    ];

    $form['heading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading'),
      '#description' => $this->t('Heading of the block.'),
      '#default_value' => isset($config['heading']) ? $config['heading'] : '',
    ];

    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text'),
      '#description' => $this->t('Text of the block.'),
      '#default_value' => isset($config['text']) ? $config['text'] : '',
    ];

    $form['image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Image'),
      '#description' => $this->t('Image of the block.'),
      '#default_value' => isset($config['image']) ? $config['image'] : '',
      '#upload_validators' => [
        'file_validate_is_image' => [],
        'file_validate_image_resolution' => ['1280x1280', '240x240'],
      ],
      '#upload_location' => 'public://kic_front/category_header',
    ];

    return $form;
  }

  /**
   * Adds block type-specific submission handling for the block form.
   *
   * Note that this method takes the form structure and form state for the full
   * block configuration form as arguments, not just the elements defined in
   * BlockPluginInterface::blockForm().
   *
   * @param array $form
   *   The form definition array for the full block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\Core\Block\BlockPluginInterface::blockForm()
   * @see \Drupal\Core\Block\BlockPluginInterface::blockValidate()
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['kicker'] = $values['kicker'];
    $this->configuration['heading'] = $values['heading'];
    $this->configuration['text'] = $values['text'];

    $old_fid = $this->configuration['image'][0] ?? 0;
    $new_fid = $values['image'][0] ?? 0;
    /*
     * Remove the old file.
     * Basically we can assign usages to the files and keeping and deleting
     * files should be done based on the usages. But usages are intended only
     * for entities and a block is not an entity, so this does not work here
     * and we keep order manually.
     */
    if ($old_fid !== 0 && $old_fid != $new_fid) {
      $old_file = File::load($old_fid);
      if ($old_file) {
        $old_file->delete();
      }
    }
    $this->configuration['image'] = $values['image'] ?? [];
    // Make the new file permanent.
    $new_file = File::load($new_fid);
    if ($new_file) {
      $new_file->setPermanent();
      $new_file->save();
    }
  }

}
