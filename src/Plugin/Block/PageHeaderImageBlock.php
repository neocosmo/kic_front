<?php

namespace Drupal\kic_front\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a 'Image' block.
 *
 * @Block(
 *   id = "kic_front_page_header_image_block",
 *   admin_label = @Translation("Page Header Image"),
 *   category = @Translation("Page"),
 * )
 */
class PageHeaderImageBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $element = [
      '#theme' => 'block',
      'content' => [],
      '#id' => 'kic_front_page_header_image_block',
      '#plugin_id' => $this->getPluginId(),
      '#base_plugin_id' => $this->getBaseId(),
      '#derivative_plugin_id' => $this->getDerivativeId(),
      '#configuration' => $this->getConfiguration(),
      '#attributes' => [
        'class' => ['page__header_image', 'page-header-image'],
      ],
    ];

    $fid = $config['image'][0] ?? 0;
    $image_file = File::load($fid);
    if ($image_file) {
      if (!empty($config['image_style'])
        && $config['image_style'] !== 'none') {

        $element['content']['image'] = [
          '#theme' => 'image_style',
          '#style_name' => $config['image_style'],
          '#uri' => $image_file->getFileUri(),
          '#weight' => 0,
          '#attributes' => [
            'class' => ['page-header-image__image'],
          ],
        ];
      }
      else {
        $element['content']['image'] = [
          '#theme' => 'image',
          '#uri' => $image_file->getFileUri(),
          '#weight' => 0,
        ];
      }
    }

    if (!empty($config['heading'])) {
      $element['content']['heading'] = [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#attributes' => [
          'class' => ['page-header-image__heading'],
        ],
        '#value' => $config['heading'],
      ];
    }
    if (!empty($config['subheading'])) {
      $element['content']['subheading'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => ['page-header-image__subheading'],
        ],
        '#value' => $config['subheading'],
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Image'),
      '#description' => $this->t('Image file to display.'),
      '#default_value' => isset($config['image']) ? $config['image'] : '',
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg', 'png', 'svg'],
      ],
      '#upload_location' => 'public://kic_front/image',
    ];

    $image_styles = ImageStyle::loadMultiple();
    $image_style_options = [
      'none' => $this->t('None'),
    ];
    $image_style_options = array_reduce($image_styles, function ($carry, $item) {
      $carry[$item->getName()] = $item->label();
      return $carry;
    }, $image_style_options);
    $form['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Image Style'),
      '#description' => $this->t('The image style used for the image.'),
      '#default_value' => $config['image_style'] ?? 'none',
      '#options' => $image_style_options,
    ];

    $form['heading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading'),
      '#description' => $this->t('Heading of the block.'),
      '#default_value' => $config['heading'] ?? '',
    ];

    $form['subheading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subheading'),
      '#description' => $this->t('Subheading of the block.'),
      '#default_value' => $config['subheading'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();

    $old_fid = $this->configuration['image'][0] ?? 0;
    $new_fid = $values['image'][0] ?? 0;
    /*
     * Remove the old file.
     * Basically we can assign usages to the files and keeping and deleting
     * files should be done based on the usages. But usages are intended only
     * for entities and a block is not an entity, so this does not work here
     * and we keep order manually.
     */
    if ($old_fid !== 0 && $old_fid != $new_fid) {
      $old_file = File::load($old_fid);
      if ($old_file) {
        $old_file->delete();
      }
    }
    $this->configuration['image'] = $values['image'] ?? [];
    // Make the new file permanent.
    $new_file = File::load($new_fid);
    if ($new_file) {
      $new_file->setPermanent();
      $new_file->save();
    }

    $this->configuration['image_style'] = $values['image_style'] ?? '';
    $this->configuration['heading'] = $values['heading'] ?? '';
    $this->configuration['subheading'] = $values['subheading'] ?? '';
  }

}
