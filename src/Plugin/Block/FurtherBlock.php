<?php

namespace Drupal\kic_front\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Provides a 'Further' block.
 *
 * @Block(
 *   id = "kic_front_further_block",
 *   admin_label = @Translation("KIC Frontpage Further"),
 *   category = @Translation("Frontpage"),
 * )
 */
class FurtherBlock extends BlockBase implements BlockPluginInterface {

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build() {
    $config = $this->getConfiguration();

    $element = [
      '#theme' => 'kic_front_further',
      '#heading' => $config['heading'] ?? '',
      '#text' => $config['text'] ?? '',
      '#register_link' => [
        '#type' => 'link',
        '#title' => $config['register_text'] ?? $this->t('Register now'),
        '#url' => Url::fromRoute('user.register', [], [
          'attributes' => [
            'class' => ['btn', 'hover-bg-green', 'wide'],
          ],
        ]),
      ],
      '#discover_link' => [
        '#type' => 'link',
        '#title' => $config['discover_text'] ?? $this->t('Discover courses'),
        '#url' => Url::fromRoute('view.overview.page_overview_general', [], [
          'attributes' => [
            'class' => ['btn', 'hover-bg-green', 'wide'],
          ],
        ]),
      ],
    ];
    return $element;
  }

  /**
   * Returns the configuration form elements specific to this block plugin.
   *
   * Blocks that need to add form elements to the normal block configuration
   * form should implement this method.
   *
   * @param array $form
   *   The form definition array for the block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The renderable form array representing the entire configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['heading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading'),
      '#description' => $this->t('Heading of the block.'),
      '#default_value' => isset($config['heading']) ? $config['heading'] : '',
    ];

    $form['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text'),
      '#description' => $this->t('Text of the block.'),
      '#default_value' => isset($config['text']) ? $config['text'] : '',
    ];

    $form['register_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Register-Button-Text'),
      '#description' => $this->t('Text of the "register"-button.'),
      '#default_value' => isset($config['register_text']) ? $config['register_text'] : '',
    ];

    $form['discover_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Discover-Button-Text'),
      '#description' => $this->t('Text of the "discover"-button.'),
      '#default_value' => isset($config['discover_text']) ? $config['discover_text'] : '',
    ];

    return $form;
  }

  /**
   * Adds block type-specific submission handling for the block form.
   *
   * Note that this method takes the form structure and form state for the full
   * block configuration form as arguments, not just the elements defined in
   * BlockPluginInterface::blockForm().
   *
   * @param array $form
   *   The form definition array for the full block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\Core\Block\BlockPluginInterface::blockForm()
   * @see \Drupal\Core\Block\BlockPluginInterface::blockValidate()
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['heading'] = $values['heading'];
    $this->configuration['text'] = $values['text'];
    $this->configuration['discover_text'] = $values['discover_text'];
    $this->configuration['register_text'] = $values['register_text'];
  }

}
