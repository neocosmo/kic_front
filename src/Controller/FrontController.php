<?php

namespace Drupal\kic_front\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\file\Entity\File;
/**
 * Frontpage controller class.
 */
class FrontController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content() {
    return [];
  }

  /**
   * Returns a ajax callback.
   *
   * @return array
   *   A simple response.
   */
  public function load_video() {
    $response = new AjaxResponse();
    $markup = '';
    $machine_name = 'kic_theme_kicfrontpagevideo';

    $block = \Drupal::entityTypeManager()
      ->getStorage('block')
      ->load($machine_name);
    $settings = $block->get('settings');
    if (!empty($url = $settings['video'])) {
      $url_parts = parse_url($url);
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $video = [
        '#theme' => 'video_embed_iframe',
        '#url' => $url_parts['scheme'] . '://' . $url_parts['host'] . $url_parts['path'],
        '#query' => [
          'autoplay' => 1,
          'rel' => '0',
          'cc_load_policy' => 1,
          'cc_lang_pref' => $language,
        ],
        '#attributes' => [
          'width' => 'auto',
          'height' => 'auto',
          'frameborder' => '0',
          'allowfullscreen' => 'allowfullscreen',
          'allow' => 'autoplay',
        ],
      ];

      if ($video) {
        $markup = \Drupal::service('renderer')->render($video);
      }
    }
    $response->addCommand(new InvokeCommand(NULL, 'frontVideoCallback', ['']));
    return $response->addCommand(new PrependCommand('#front-video-ajax', $markup));

  }
}
