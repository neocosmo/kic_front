<?php

namespace Drupal\kic_front\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Change the core's default path for `/contact` so that we can use it as an
    // alias.
    if ($route = $collection->get('contact.site_page')) {
      $route->setPath('/contact');
    }
  }
}
