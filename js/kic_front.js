/**
 * @file
 * Global utilities.
 *
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.kic_front = {
    attach: function (context, settings) {
      $('.use-ajax.front-image-wrapper').click(function() {
        $('.front-video-wrapper').show();
      });
    }
  };
  $.fn.frontVideoCallback = function(data) {
    setTimeout(function() {
      $('#front-video-player').on('ended', function() {
        $('.front-video-wrapper').hide();
      });
    }, 2000);
    $('.front-video-close').click(function() {
      $('.front-video-wrapper').hide();
      $('#front-video-ajax').empty();
    });
  };

})(jQuery, Drupal);
